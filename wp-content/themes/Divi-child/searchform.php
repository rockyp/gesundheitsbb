<?php
/**
 * Template for displaying search forms in Divi Child
 *
 * @package WordPress
 * @subpackage Divi Child
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>
<div class="mobile-search">
	<span id="icon-holder-search">
	  <span class="icon-search"></span>	
	</span>
	<form role="search" method="get" class="search-form" id="search-form-header" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Suchbegriff eingeben', 'placeholder', 'divi' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search-submit"><span class="icon-search"></span>
		<!-- <span class="text-search">Suche</span> -->
	</button>
</form>
</div>