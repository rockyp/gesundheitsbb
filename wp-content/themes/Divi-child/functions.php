<?php 
// Add Images sizes
add_image_size( 'download-size', 100, 110, true);
add_image_size( 'gallery-size', 470, 420, true);

//* TN Dequeue Styles - Remove Google Fonts from Divi Theme
add_action( 'wp_print_styles', 'tn_dequeue_divi_fonts_style' );
function tn_dequeue_divi_fonts_style() {
wp_dequeue_style( 'divi-fonts' );
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'extended-css', get_stylesheet_directory_uri() . '/extended.css' );
	wp_enqueue_style( 'icomoon-css', get_stylesheet_directory_uri() . '/assets/fonts/font.css' );
	
	wp_enqueue_script( 'flexslider-js', get_stylesheet_directory_uri().'/flexslider/jquery.flexslider-min.js', '', '', true);
	wp_enqueue_style( 'flexslider-css', get_stylesheet_directory_uri().'/flexslider/flexslider.css');
	wp_enqueue_script( 'isotope', get_stylesheet_directory_uri().'/assets/js/isotope.pkgd.min.js', '', '', true);
	wp_enqueue_script( 'magnetic', get_stylesheet_directory_uri(). '/assets/js/jquery.magnific-popup.min.js','','', true);
	wp_enqueue_style( 'magnetic-css', get_stylesheet_directory_uri().'/assets/css/magnific-popup.min.css');
	wp_enqueue_script( 'nice-js', get_stylesheet_directory_uri().'/assets/js/jquery.nice-select.js');
	wp_enqueue_style( 'nice-css', get_stylesheet_directory_uri().'/assets/css/nice-select.css');
	wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri().'/assets/js/custom.js', '', '', true);
}

remove_filter ('acf_the_content', 'wpautop');


// filter the Gravity Forms button type
// add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
// function form_submit_button( $button, $form ) {
// 	return "<button class='et_pb_button button et_submit_button' id='gform_submit_button_{$form['id']}'><span>".$form[button][text]."</span></button>";
// }


function gbb_create_post_type() {

	register_post_type( 'project',
		array(
			'labels' => array(
				'name' => __( 'Projects' ),
				'singular_name' => __( 'Project' )
			),
			'public' => true,
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => 'buendnis'),
		)
	);



	register_post_type( 'event',
		array(
			'labels' => array(
				'name' => __( 'Events' ),
				'singular_name' => __( 'Event' )
			),
			'public' => true,
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
			'has_archive' => true,
		)
	);

	register_post_type( 'landkreis',
		array(
			'labels' => array(
				'name' => __( 'Landkreis' ),
				'singular_name' => __( 'Landkreis' )
			),
			// 'exclude_from_search' => true,
			'public' => true,
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
			'has_archive' => true,
		)
	);



}
add_action( 'init', 'gbb_create_post_type' );


register_taxonomy( 'events', array('event'), array(
	'hierarchical' => true, 
	'label' => 'Event Categories', 
	'singular_label' => 'event', 
	'rewrite' => array( 'slug' => 'events', 'with_front'=> false )
)
);

register_taxonomy_for_object_type( 'events', 'event' );

function coordinating_body_news_func( $atts ) { ob_start(); 

	$args_coordinating_body_news = array(
		'posts_per_page'    => 3,
		'post_type'         => 'post',
		'post_status'       => 'publish',
		'order'             => 'DESC'
	);
	$query_coordinating_body_news = new WP_Query( $args_coordinating_body_news );     
	while ( $query_coordinating_body_news->have_posts() ) { $query_coordinating_body_news->the_post();
		$category = get_the_category(); 
		?>
		<div class="coordinating-body-news">
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></br>
			

			<span class="icon-date"></span> <?php 
                        // get raw date
						$dater =  get_the_date('d-m-Y');

						setlocale(LC_ALL, 'de_DE');
 
						?>
						<!-- <span class="day"><?php echo   strftime("%d", strtotime($dater)); ?></span> -->
                        <span class="month"><?php echo utf8_encode(strftime("%B", strtotime($dater))); ?></span>
                        
                        <span class="year"><?php echo date("Y", strtotime($dater));?></span> <!-- | <span class="post-category"><?php echo $category[0]->cat_name; ?></span> -->
			<?php //the_excerpt(); ?>
		</div>   
		<?php }  
		wp_reset_query();
		return ob_get_clean(); 
	} 
	add_shortcode( 'coordinating-body-news', 'coordinating_body_news_func' );



	function home_events_function( $atts ) { ob_start(); 

		$args_home_events = array(
			'posts_per_page'    => 3,
			'post_type'         => 'event',
			'post_status'       => 'publish',
			'meta_key'			=> 'event_date',
			'orderby'			=> 'meta_value',
			'order'				=> 'ASC',
			'meta_query' => array(
				array(
					'key' => 'event_date',
					'value' => date("Ymd"),
										'compare' => '>=',          // method of comparison
									)
			),
		);
		$query_home_events = new WP_Query( $args_home_events );     

		while ( $query_home_events->have_posts() ) { $query_home_events->the_post();
			$past_post_terms = get_the_terms( get_the_ID(), 'events');
					$event_cat = ""; //initialize the strng that will contain the terms
					if (is_array($past_post_terms)) {
							foreach ( $past_post_terms as $past_post_term ) { // for each term 
								$event_cat .= $past_post_term->name.' '; //create a string that has all the slugs 
							}
						}
						$date = get_field('event_date', false, false);
						$end_date = get_field('event_end_date', false, false);
						setlocale(LC_ALL, 'de_DE');

						?>
						<div class="coordinating-body-news">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></br>
							<span class="dater-group">
								<span class="icon-date"></span> 
							<span><?php echo  strftime("%d", strtotime($date));  if(get_field('event_end_date')){ ?>-<?php echo  strftime("%d", strtotime($end_date)); } ?></span>.
							<span><?php echo utf8_encode(strftime("%B", strtotime($date))); ?></span>
							<span><?php echo date("Y", strtotime($date));?></span>
							</span>
							<?php if(get_field('start_time')){?>
							<span class="time-group">

								<span class="icon-time"></span>
								<span class="start-time"><?php 
								if(get_field('event_end_date')){ echo strftime("%d", strtotime($date)).' '.strftime("%b", strtotime($date)); ?>,<?php  } ?> 
								<?php the_field('start_time');?>
							</span>
							<?php if(get_field('start_time') && get_field('end_time')){?>
							<span>-</span>
							<?php } ?>
							<span class="end-time"><?php
							if(get_field('event_end_date')){
								echo strftime("%d", strtotime($end_date)).' '.strftime("%b", strtotime($end_date)); ?>,<?php
							} ?>
							<?php the_field('end_time');?>

						</span>
						<span class="hour">Uhr</span>
					</span>
					<?php } ?> 
					<?php if($event_cat){?>
					<span class="cat-group post-category"><?php echo ucfirst($event_cat); ?></span>
					<?php }?>
				</div>   
				<?php }  
				wp_reset_query();
				return ob_get_clean(); 
			} 
			add_shortcode( 'event_list_home', 'home_events_function' );

add_theme_support( 'post-thumbnails', array( 'download' ) ); // Add it for posts


/**yearly archive blog **/
function my_limit_archives( $args ) {
    $args['type'] = 'yearly';
    return $args;
}

add_filter( 'widget_archives_args', 'my_limit_archives' );
add_filter( 'widget_archives_dropdown_args', 'my_limit_archives' );


function eventstab_function($atts){
	ob_start();

	?>
	<div class="easytabs">
		<div id="sort-wrapper">
			<div id="toggler-sort">Sortiere nach <span class="sort-name"></span></div>
			<div id="sorts" style="display: none;" class="button-group sort-by-button-group">
				<button class="btn btn-primary" data-sort-value="hidden-date" data-sort-direction="asc">Datum: nächste zuerst</button>
				<button class="btn btn-primary" data-sort-value="hidden-date" data-sort-direction="desc">Datum: späteste zuerst</button>
				<button class="btn btn-primary" data-sort-value="landkreis" data-sort-direction="asc">nach Landkreis A-Z</button>
				<button class="btn btn-primary" data-sort-value="landkreis" data-sort-direction="desc">nach Landkreis Z-A</button>
				<button class="btn btn-primary" data-sort-value="name" data-sort-direction="asc">Veranstaltungstitel A-Z</button>
				<button class="btn btn-primary" data-sort-value="name" data-sort-direction="desc">Veranstaltungstitel Z-A</button>
			</div>
		</div>
		<!-- .sort-wrapper -->

		<div id="demoTab">
			
			
<?php /*			<ul id="filters" class="filters clearfix">
				<li><a href="#" data-filter="*" class="selected">Alle Kategorien</a></li>
				<?php 
							 $terms = get_terms("events"); // get all categories, but you can use any taxonomy
							 $counter = count($terms); //How many are they?
								 if ( $counter > 0 ){  //If there are more than 0 terms
									 foreach ( $terms as $term ) {  //for each term:
									 	echo "<li><a href='#' data-filter='.".$term->slug."'>" . $term->name . "</a></li>\n";
									 //create a list item with the current term slug for sorting, and name for label
									 }
									}?>

								</ul> 
*/ ?>


								<?php 
								$args = array( 
									'post_type' => 'event', 
									'posts_per_page' => -1,
									'post_status'       => 'publish',
									'meta_key'			=> 'event_date',
									'orderby'			=> 'meta_value',
									'order'				=> 'ASC',
									'meta_query' => array(
										array(
											'key' => 'event_date',
											'value' => date("Ymd"),
											'compare' => '>=',          // method of comparison
										)
									),
								); 


								$the_query = new WP_Query( $args ); //Check the WP_Query docs to see how you can limit which posts to display 

								?>
								<?php if ( $the_query->have_posts() ) : ?>
									<div id="isotope-list" class="grid">
										<?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
											 $past_post_terms = get_the_terms( get_the_ID(), 'events');  //Get the terms for this particular item
											 $termsString2 = ""; //initialize the strng that will contain the terms
											 if (is_array($past_post_terms)) {
												 foreach ( $past_post_terms as $past_post_term ) { // for each term 
												 $termsString2 .= $past_post_term->slug.' '; //create a string that has all the slugs 
												}
											}
											?> 
											<div class="<?php echo $termsString2; ?>item element-item"> 
												<?php if(get_field('event_date')){
												 // get raw date
													$date = get_field('event_date', false, false);
													$end_date = get_field('event_end_date', false, false);
													setlocale(LC_ALL, 'de_DE');
													?>
													<span class="hidden-date" style="display: none;">
														<?php echo $date;?>
													</span>
													<div class="event-date <?php if(get_field('event_end_date')){ echo 'with-enddate'; } else { }?>">
														<span class="month"><?php echo utf8_encode(strftime("%b", strtotime($date))); ?></span>
														<span class="day"><?php echo  strftime("%d", strtotime($date));if(get_field('event_end_date')){ ?>-<?php echo  strftime("%d", strtotime($end_date)); } ?></span>
														<span class="year"><?php echo date("Y", strtotime($date));?></span>
													</div>
													<?php } ?>
													<div class="slider-desc flexer">
														<a href="<?php the_permalink();?>">Mehr</a>
														<h4 class="name"><?php the_title(); ?></h4>
														<div class="date-info">
															<?php if(get_field('start_time')){?>
															<span class="time-group">
																
																<span class="icon-time"></span>
																<span class="start-time"><?php 
																if(get_field('event_end_date')){ echo strftime("%d", strtotime($date)).' '.strftime("%b", strtotime($date)); ?>,<?php  } ?> 
																<?php the_field('start_time');?>
															</span>
															<?php if(get_field('start_time') && get_field('end_time')){?>
															<span>-</span>
															<?php } ?>
															<span class="end-time"><?php
															if(get_field('event_end_date')){
																echo strftime("%d", strtotime($end_date)).' '.strftime("%b", strtotime($end_date)); ?>,<?php
															} ?>
															<?php the_field('end_time');?>

														</span>
														<span class="hour">Uhr</span>
													</span>
													<?php } 
													if(get_field('location')){?>
													<span class="location-group">
														<span class="icon-location-map"></span>
														<span class="location"><?php the_field('location');?></span>
													</span>
													<?php } ?>


													<?php if($termsString2){?>
													<span class="cat-group"><?php echo ucfirst($termsString2); ?></span>
													<?php } ?>




												</div>

												<?php $projects = get_field('projects');
															$project_id = $projects->ID;?>

															<?php  $args_projects = array(   'post_type' => 'project',
																'posts_per_page' => 1,
																'p' => $project_id,
															);
															$loop_projects = new WP_Query( $args_projects );
												 while( $loop_projects->have_posts() ) : $loop_projects->the_post();
												$landkreis = get_field('landkreis');
												if($landkreis->post_title):?>
												<p class="event-excerpt">
													<?php echo $landkreis->post_title;?>
												</p>

											<?php endif;

										endwhile;
										?>
									</div> <!-- end item -->
								</div> <!-- end item -->
							<?php endwhile;  wp_reset_postdata();?>
						</div> <!-- end isotope-list -->
					<?php endif;?>

				</div>
			</div>  
			<?php return ob_get_clean();
		}

		add_shortcode('easy_tabs', 'eventstab_function');


				// events tab starts 
		function past_event_tabs($atts){
			ob_start();

			?>
			<div class="easytabs">
				<div>

					<ul id="filters" class="filters clearfix">
						<li><a href="#" data-filter="*" class="selected">Alle Kategorien</a></li>
						<?php 
				 $terms = get_terms("events"); // get all categories, but you can use any taxonomy
				 $count = count($terms); //How many are they?
					 if ( $count > 0 ){  //If there are more than 0 terms
						 foreach ( $terms as $term ) {  //for each term:
						 	echo "<li><a href='#' data-filter='.".$term->slug."'>" . $term->name . "</a></li>\n";
						 //create a list item with the current term slug for sorting, and name for label
						 }
						}?>

					</ul> 

					<?php 
					$args2 = array( 
						'post_type' => 'event', 
						'posts_per_page' => -1,
						'post_status'       => 'publish',
						'meta_key'			=> 'event_date',
						'orderby'			=> 'meta_value',
						'order'				=> 'DESC',
						'meta_query' => array(
							array(
								'key' => 'event_date',
								'value' => date("Ymd"),
					            'compare' => '<',       // method of comparison
					        )
						),
					); 


					$the_query = new WP_Query( $args2 ); //Check the WP_Query docs to see how you can limit which posts to display 

					?>
					<?php if ( $the_query->have_posts() ) : ?>
						<div id="isotope-list">
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
								 $post_terms = get_the_terms( get_the_ID(), 'events');  //Get the terms for this particular item
								 $termsString = ""; //initialize the strng that will contain the terms
								 if (is_array($post_terms)) {
									 foreach ( $post_terms as $post_term ) { // for each term 
									 $termsString .= $post_term->slug.' '; //create a string that has all the slugs 
									}
								}
								?> 
								<div class="<?php echo $termsString; ?>item"> 
									<?php if(get_field('event_date')){
                        // get raw date
										$date2 = get_field('event_date', false, false);
										$end_date = get_field('event_end_date', false, false);
										setlocale(LC_ALL, 'de_DE');
										?>
										<div class="event-date">
											
											<span class="month"><?php echo utf8_encode(strftime("%b", strtotime($date2))); ?></span>
											<span class="day"><?php echo  strftime("%d", strtotime($date2));  if(get_field('event_end_date')){ ?>-<?php echo  strftime("%d", strtotime($end_date)); } ?></span>
											<span class="year"><?php echo date("Y", strtotime($date2));?></span>


										</div>
										<?php } ?>
										<div class="slider-desc">
											<h4><?php the_title(); ?></h4>
											<div class="date-info">
												<?php if(get_field('start_time')){?>
												<span class="time-group">
													<span class="icon-time"></span>
													<span class="start-time"><?php 
													if(get_field('event_end_date')){ echo strftime("%d", strtotime($date2)).' '.strftime("%b", strtotime($date2)); ?>,<?php  } ?> 
													<?php the_field('start_time');?>
												</span>
												<?php if(get_field('start_time') && get_field('end_time')){?>
												<span>-</span>

												<?php } ?>
												<span class="end-time"><?php
												if(get_field('event_end_date')){
													echo strftime("%d", strtotime($end_date)).' '.strftime("%b", strtotime($end_date)); ?>,<?php
												} ?>
												<?php the_field('end_time');?>	 	
											</span>
											<span class="hour">Uhr</span>
										</span>
										<?php } 
										if(get_field('location')){?>
										<span class="location-group">
											<span class="icon-location-map"></span>
											<span class="location"><?php the_field('location');?></span>
										</span>
										<?php } ?>
										<span class="cat-group"><?php echo ucfirst($termsString); ?></span>
									</div>
									<?php echo wp_trim_words( get_the_content(), 18, '...');?>
									<a href="<?php the_permalink()?>">Mehr</a>
								</div> <!-- end item -->
							</div> <!-- end item -->
						<?php endwhile;  ?>
					</div> <!-- end isotope-list -->
				<?php endif; wp_reset_query(); ?>

			</div>
		</div>
		<?php return ob_get_clean();
	}

	add_shortcode('past_event_tabs', 'past_event_tabs');

				// events tab ends 

				// project search starts
	function project_search_function( $atts ) { ob_start(); 

		?>
		<div class="container">
			<div class="downloads_list">
				<h2>BÜNDNISSE VON A BIS Z</h2>
				<div id="searchblock" style="width: 100%; max-width: 800px; margin: 0 auto;">

					<form>
						<div class="typeahead__container">
							<div class="typeahead__field">

								<span class="typeahead__query">
									<input class="js-typeahead"
									name="q"
									type="search"
									autofocus
									autocomplete="off" placeholder="Name des Bündnisses hier eingeben">
								</span>
								<span class="typeahead__button">
									<button type="submit">
										<span class="typeahead__search-icon"></span>
									</button>
								</span>

							</div>
						</div>
					</form>



					<div id="links">


						<?php 
						$args_project = array(
							'posts_per_page'    => -1,
							'post_type'         => 'project',
							'post_status'       => 'publish',
							'order'             => 'DESC'
						);
						$query_downloads = new WP_Query( $args_project );     
						while ( $query_downloads->have_posts() ) { $query_downloads->the_post();
							$category = get_the_category(); 
							?>
							<div id="1" data-title="<?php the_title();?>" data-url="<?php echo get_the_permalink();?>"></div>

							<?php }  ?>



						</div>

					</div>
					<!--search block-->

				</div>
			</div>
			<?php 
			wp_reset_query();
			return ob_get_clean(); 
		} 
		add_shortcode( 'project_search', 'project_search_function' );

				// project search ends

		function project_by_district_function( $atts ) { 
			ob_start(); 
			?>

			<div class="projectlist">
				<div class="container">
					<div id="karte">
						<span id="k_barnim" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1905" onmouseover="show_me('barnim')" onmouseout="hide_all()" class="k_show">
							<span class="group-holder">
								<span class="title">Barnim</span>


								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 346
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>



							</span>
						</span>
						<span id="k_dahme-spree" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1904" onmouseover="show_me('dahme-spree')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Dahme-Spreewald</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 349
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_elbe-elster" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1903" onmouseover="show_me('elbe-elster')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Elbe-Elster</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 350
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_havelland" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1902" onmouseover="show_me('havelland')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Havelland</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 351
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_maerkisch-oderland" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1901" onmouseover="show_me('maerkisch-oderland')" onmouseout="hide_all()" class=""> 
							<span class="group-holder">
								<span class="title">Märkisch-Oderland</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 352
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_oberhavel" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1900" onmouseover="show_me('oberhavel')" onmouseout="hide_all()" class=""> 
							<span class="group-holder">
								<span class="title">Oberhavel</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 353
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_oberspreewald-lausitz" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1899" onmouseover="show_me('oberspreewald-lausitz')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Oberspreewald-Lausitz</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 354
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_oder-spree" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1898" onmouseover="show_me('oder-spree')" onmouseout="hide_all()" title="Titletext" class="">
							<span class="group-holder">
								<span class="title">Oder-Spree</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 355
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_ostprignitz-ruppin" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1897" onmouseover="show_me('ostprignitz-ruppin')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Ostprignitz-Ruppin</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 356
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_potsdam-mittelmark" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1896" onmouseover="show_me('potsdam-mittelmark')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Potsdam-Mittelmark</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 357
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_prignitz" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1895" onmouseover="show_me('prignitz')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Prignitz</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 358
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_spree-neisse" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1894" onmouseover="show_me('spree-neisse')" onmouseout="hide_all()" class=""> 
							<span class="group-holder">
								<span class="title">Spree-Neiße</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 359
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_teltow-flaeming" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1893" onmouseover="show_me('teltow-flaeming')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Teltow-Fläming</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 360
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_uckermark" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1892" onmouseover="show_me('uckermark')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Uckermark</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 361
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_brandenburg" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1891" onmouseover="show_me('brandenburg')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Stadt Brandenburg an der Havel</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 362
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_cottbus" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1890" onmouseover="show_me('cottbus')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Stadt Cottbus</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 363
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'"><h4>' . get_the_title() . '</h4></a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_frankfurt" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1889" onmouseover="show_me('frankfurt')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Stadt Frankfurt (Oder)</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 364
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'"><h4>' . get_the_title() . '</h4></a>';

									}

								} 

								?>
							</span>
						</span>
						<span id="k_potsdam" href="http://www.familienbuendnisse-land-brandenburg.de/index.php?id=1888" onmouseover="show_me('potsdam')" onmouseout="hide_all()" class="">
							<span class="group-holder">
								<span class="title">Stadt Potsdam</span>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 365
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {

									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'"><h4>' . get_the_title() . '</h4></a>';

									}

								} 

								?>
							</span>
						</span>
					</div>

					<div id="liste-karte">

						<div id="accordion">

							<div class="l_barnim locationinfo">
							<h3 id="l_barnim">Barnim</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 346
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 346,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>

							</div>
                            </div>

<div class="l_dahme-spree locationinfo">
							<h3 id="l_dahme-spree">Dahme-Spreewald</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 349
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 349,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>


<div class="l_elbe-elster locationinfo">
							<h3 id="l_elbe-elster" >Elbe-Elster</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 350
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

								<?php $args = array('p' => 350,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>


<div class="l_havelland locationinfo">
							<h3 id="l_havelland">Havelland</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 351
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

								<?php $args = array('p' => 351,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
                            
                            <div class="l_maerkisch-oderland locationinfo">

							<h3 id="l_maerkisch-oderland">Märkisch-Oderland</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 352
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 352,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
                            <div class="l_oberhavel locationinfo">
                            
							<h3  id="l_oberhavel">Oberhavel </h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 353
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 353,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>

<div class="l_oberspreewald-lausitz locationinfo">

							<h3 id="l_oberspreewald-lausitz">Oberspreewald-Lausitz</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 354
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

								<?php $args = array('p' => 354,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>


<div class="l_oder-spree locationinfo">
							<h3 id="l_oder-spree">Oder-Spree</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 355
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 355,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
                            
                            <div class="l_ostprignitz-ruppin locationinfo">
							<h3 id="l_ostprignitz-ruppin">Ostprignitz-Ruppin</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 356
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

								<?php $args = array('p' => 356,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
                            
                            <div class="l_potsdam-mittelmark locationinfo">
                            
							<h3  id="l_potsdam-mittelmark">Potsdam-Mittelmark</h3>

							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 357
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

								<?php $args = array('p' => 357,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>

<div class="l_prignitz locationinfo">
							<h3 id="l_prignitz">  Prignitz</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 358
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 358,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>


<div class="l_spree-neisse locationinfo">
							<h3 i id="l_spree-neisse" >Spree-Neiße </h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 359
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 359,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
                            
                            
                            <div class="l_teltow-flaeming locationinfo">
                            
							<h3  id="l_teltow-flaeming">Teltow-Fläming</h3>
                            
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 360
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 360,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
                            
                            <div class="l_uckermark locationinfo">
                            
							<h3  id="l_uckermark">Uckermark</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 361
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 361,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>

							
                            <div class="l_brandenburg locationinfo">
                            <h3  id="l_brandenburg">Stadt Brandenburg an der Havel</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 362
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 362,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
							<div class="l_cottbus locationinfo">
                            <h3  id="l_cottbus" >Stadt Cottbus</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 363
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 363,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
							<div class="l_frankfurt  locationinfo">
                            <h3  id="l_frankfurt" >Stadt Frankfurt (Oder)</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 364
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 364,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>
                            
							<div class="l_potsdam locationinfo">
                            <h3 id="l_potsdam" >Stadt Potsdam</h3>
							<div>

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 365
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								<?php $args = array('p' => 365,'post_type' => 'landkreis');
								$my_posts = new WP_Query($args);
								while ( $my_posts->have_posts() ) {
									$my_posts->the_post(); 
									$img = get_the_post_thumbnail_url();
									echo '<img src="'.$img.'" class="img-responsive"/>';

								}?>
							</div>
                            </div>



						</div>
					</div>


					


				</div>
			</div>   
			<?php 
			return ob_get_clean(); 
		} 
		add_shortcode( 'project_search_district', 'project_by_district_function' );

		function event_calendar(){ ob_start(); ?>
		<div id='eventCalendarLocaleFile'></div>
		<?php return ob_get_clean();  }
		add_shortcode('event_calendar_code', 'event_calendar');


		function func_events_archive( $atts ) {
			ob_start(); 
			?>
			<h4><?php _e('VERANSTALTUNGEN ARCHIVE'); ?></h4>
			<ul class="styled">	
				<?php 
				wp_get_archives_cpt( 'post_type=event' ); 
				echo "</ul>";
				return ob_get_clean(); 
			}
			add_shortcode( 'events_archive', 'func_events_archive' );


// Register and load the widget
			function eventarchive_load_widget() {
				register_widget( 'eventarchive_widget' );
			}
			add_action( 'widgets_init', 'eventarchive_load_widget' );

// Creating the widget 
			class eventarchive_widget extends WP_Widget {

				function __construct() {
					parent::__construct(

	// Base ID of your widget
						'wpb_widget', 

	// Widget name will appear in UI
						__('Event Archive Widget', 'wpb_widget_domain'), 

	// Widget description
						array( 'description' => __( 'Event Archive Widget', 'wpb_widget_domain' ), ) 
					);
				}

	// Creating widget front-end

				public function widget( $args, $instance ) {

					$title = apply_filters( 'widget_title', $instance['title'] );

		// before and after widget arguments are defined by themes
					echo $args['before_widget'];
					if ( ! empty( $title ) )
						?>
					<h4><?php _e('VERANSTALTUNGEN ARCHIVE'); ?></h4>
					<ul class="styled">	
						<?php 
		// This is where you run the code and display the output
						wp_get_archives_cpt( 'post_type=event' ); 
						echo '</ul>';
						echo $args['after_widget'];

					}


} // Class wpb_widget ends here


function et_password_form_new() {
	$pwbox_id = rand();

	$form_output = sprintf(
		'<div class="et_password_protected_form">
		<h1>%1$s</h1>
		<p>%2$s:</p>
		<form action="%3$s" method="post">
		<p><label for="%4$s">%5$s: </label><input name="post_password" id="%4$s" type="password" size="20" maxlength="20" /></p>
		<p><button type="submit" class="et_pb_button">%6$s</button></p>
		</form>
		</div>',
		esc_html__( 'Dieser Bereich ist passwortgeschützt.', 'Divi' ),
		esc_html__( 'Um diese Inhalte einzusehen, geben Sie bitte ein gültiges Passwort ein', 'Divi' ),
		esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ),
		esc_attr( 'pwbox-' . $pwbox_id ),
		esc_html__( 'Password', 'Divi' ),
		esc_html__( 'EINLOGGEN', 'Divi' )
	);

	$output = sprintf(
		'<div class="et_pb_section et_section_regular">
		<div class="et_pb_row">
		<div class="et_pb_column et_pb_column_4_4">
		%1$s
		</div>
		</div>
		</div>',
		$form_output
	);

	return $output;
}
add_filter( 'the_password_form', 'et_password_form_new', 20 );

function fixed_personalinfo() {
	register_sidebar( array(
		'name' => __( 'Personal Info Sidebar', 'Divi-child' ),
		'id' => 'sidebar-6',
		'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'Divi-child' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'fixed_personalinfo' );

function func_personalinfo( $atts ) {
	ob_start();
	dynamic_sidebar('sidebar-6');
	return ob_get_clean(); 
}
add_shortcode( 'main_info', 'func_personalinfo' );

// functions.php
 
add_action( 'init', 'remove_download_from_search', 99 );
 
/**
 * update_my_custom_type
 *
 * @author  Joe Sexton <joe@webtipblog.com>
 */
function remove_download_from_search() {
	global $wp_post_types;
 
	if ( post_type_exists( 'wpdmpro' ) ) {
 
		// exclude from search results
		$wp_post_types['wpdmpro']->exclude_from_search = true;
	}
}


add_filter( 'gform_ip_address', '__return_empty_string' );


function district_accordion_function(){ 
	ob_start(); 
?>

<div class="location_accordion ">
		
        
        
<button class="accordion">Barnim</button>
<div class="panel">
 <?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 346
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
								
</div>

<button class="accordion">Elbe-Elster</button>
<div class="panel">
 <?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 350
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

								
</div>

<button class="accordion">Havelland</button>
<div class="panel">
	<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 351
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

</div>


	
    
<button class="accordion">Märkisch-Oderland</button>
<div class="panel">
	<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 352
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
						
</div>



    
<button class="accordion">Oberhavel</button>
<div class="panel">

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 353
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
						
</div>

    
<button class="accordion">Oberspreewald-Lausitz</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 354
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

</div>
    
<button class="accordion">Oder-Spree</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 355
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
					
</div>
    
<button class="accordion">Ostprignitz-Ruppin</button>
<div class="panel">

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 356
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

						
</div>
    
<button class="accordion">Potsdam-Mittelmark</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 357
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>

</div>
    
<button class="accordion">Prignitz</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 358
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
				
</div>
    
<button class="accordion">Spree-Neiße</button>
<div class="panel">

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 359
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
			
</div>
    
<button class="accordion">Teltow-Fläming</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 360
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
					
</div>
    
<button class="accordion">Uckermark</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 361
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
			
</div>
    
<button class="accordion">Stadt Brandenburg an der Havel</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 362
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
		
</div>
    
<button class="accordion">Stadt Cottbus</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 363
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
					
</div>
    
<button class="accordion">Stadt Frankfurt (Oder)</button>
<div class="panel">

								<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 364
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
					
</div>

    
<button class="accordion">Stadt Potsdam</button>
<div class="panel">
<?php 
								$rd_args = array(
									'post_type' => 'project',
									'meta_key' => 'landkreis',
									'meta_value' => 365
								);

								$the_query = new WP_Query( $rd_args );	
								if ( $the_query->have_posts() ) {
									echo '<div class="projectname">';
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';

									}
									echo '</div>';
								} 

								?>
		
</div>
    
  
</div>
	
	
	<?php 
	return ob_get_clean(); 
	} 
			
	add_shortcode( 'district_accordion', 'district_accordion_function' );		
	
	

wp_schedule_event(time(), 'hourly', 'my_daily_event');

add_action('my_daily_event', 'do_this_daily');

function do_this_daily() {


	$event_args = array(
				'post_type' => 'event',
				'post_per_page'=>-1,
				'post_status'=>'publish'

			);

			$the_query = new WP_Query( $event_args );	
			
			if ( $the_query->have_posts() ) {
				
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					
					$odate = get_field('event_date');
					$newDate = date("Y-m-d", strtotime($odate));
					$today = date("Y-m-d");


					 if($today > $newDate){ 
				
					wp_update_post(
							array (
								'ID'            => get_the_ID(), // ID of the post to update
								'post_date'     => $newDate,
								'post_date_gmt' => get_gmt_from_date(get_field('event_date') )
							)
						);
					 }

				}
				
			} 


}
add_filter( 'gform_ip_address', '__return_empty_string' );
