

<?php if( is_page(104) ){
$page_slug ='newsletter';
$page_data = get_page_by_path($page_slug);
echo do_shortcode($page_data->post_content);
} else{ ?>

<div id="newsletter-portion" style="padding-bottom: 90px;">
				
</div>
<?php  }?>


<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
                    
						<?php
						
						
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix">
				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}

					echo et_get_footer_credits();
				?>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
<?php if(is_page('251')){ ?>
<!-- Core CSS File. The CSS code needed to make eventCalendar works -->
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/jQueryEventCalendar/css/eventCalendar.css">

	<!-- Theme CSS file: it makes eventCalendar nicer -->
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/jQueryEventCalendar/css/eventCalendar_theme_responsive.css">

	<!--<script src="js/jquery.js" type="text/javascript"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

				<script>
					jQuery(document).ready(function() {

						var eventsInline = [ 
							<?php  
						        $args = array( 'posts_per_page' => -1, 'post_type' => 'event', 'suppress_filters' => false );
						        $myposts = get_posts( $args );
								$totaleventparent = 0; 
						        foreach ( $myposts as $post ) : setup_postdata( $post );   
									$events_start_from = get_field('event_date');
									$desc = get_the_excerpt(); 
									$description = strip_tags($desc);
									$event_desc = substr($description, 0,20);
									$timestamp = strtotime($events_start_from)*1000; 
							?>

							{"title": "<?php echo the_title(); ?>", "date": "<?php echo $timestamp; ?>", "type": "text", "description": "", "url": "<?php echo the_permalink(); ?>"},

					        <?php endforeach; wp_reset_postdata();?>

							];



						
						jQuery("#eventCalendarLocaleFile").eventCalendar({

							// eventsjson: '<?php echo get_stylesheet_directory_uri(); ?>/jQueryEventCalendar/json/events.json.php',
							jsonData: eventsInline,
							showDescription: true,
							cacheJson: false,
							dateFormat: "DD.MM.YYYY",
							locales: {
								locale: "es",
								monthNames: [ "Januar", "Februar", "März", "April", "Mai", "Juni",
									"Juli", "August", "September", "Oktober", "November", "Dezember" ],
								dayNames: [ 'Sonntag','Montag','Dienstag','Mittwoch','Donnerstag',
									'Freitag','Samstag' ],
								dayNamesShort: [ 'So','Mo','Di','Mi','Do','Fr','Sa' ],
								txt_noEvents: "Für diesen Zeitraum gibt es keine Ereignisse",
								txt_SpecificEvents_prev: "",
								txt_SpecificEvents_after: "Ereignisse",
								txt_next: "Folgendes",
								txt_prev: "vorheriges",
								txt_NextEvents: "<h4>KOMMENDE VERANSTALTUNGEN</h4>",
								txt_GoToEventUrl: "Mehr",
								"moment": {
							        "months" : [ "Januar", "Februar", "März", "April", "Mai", "Juni",
									"Juli", "August", "September", "Oktober", "November", "Dezember" ],
							        "monthsShort" : [ "Jan", "Feb", "Mär", "Apr", "Mai", "Jun",
							                "Jul", "Aug", "Sep", "Okt", "Nov", "Dez" ],
							        "weekdays" : [ 'Sonntag','Montag','Dienstag','Mittwoch','Donnerstag',
									'Freitag','Samstag' ],
							        "weekdaysShort" : [ 'So','Mo','Di','Mi','Do','Fr','Sa' ],
							        "weekdaysMin" : [ 'So','Mo','Di','Mi','Do','Fr','Sa' ],
							        "longDateFormat" : {
							            "LT" : "H:mm",
							            "LTS" : "H:i",
							            "L" : "DD/MM/YYYY",
							            "LL" : "DD  MMMM  YYYY",
							            "LLL" : "DD  MMMM  YYYY LT",
							            "LLLL" : "dddd, DD MMMM YYYY LT"
							        },
							        "week" : {
							            "dow" : 1,
							            "doy" : 4
							        }
							    }
							}
						});
					});
				</script>
	
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/jQueryEventCalendar/js/moment.js" type="text/javascript"></script>

	<script src="<?php echo get_stylesheet_directory_uri(); ?>/jQueryEventCalendar/js/jquery.eventCalendar.js" type="text/javascript"></script>

<?php } ?>


<?php if(is_page('249') ){ ?>
 
 <!--    search starts-->
 
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/search/src/jquery.typeahead.css">

    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <!--script src="../dist/jquery.typeahead.min.js"></script-->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/search/src/jquery.typeahead.js"></script>    


    <script>

        var data = {
            countries: [
			
			
			   
			<?php 
              $args_project = array(
                  'posts_per_page'    => -1,
                  'post_type'         => 'project',
                  'post_status'       => 'publish',
                  'order'             => 'DESC'
                    );
              $query_downloads = new WP_Query( $args_project );     
              while ( $query_downloads->have_posts() ) { $query_downloads->the_post();
              $category = get_the_category(); 
              ?>
                "<?php the_title();?>",
              
          	  <?php }  ?>
			
			
				]
          
        };

        typeof $.typeahead === 'function' && $.typeahead({
            input: ".js-typeahead",
            minLength: 1,
            order: "asc",
            group: false,
            maxItemPerGroup: 3,
            groupOrder: function (node, query, result, resultCount, resultCountPerGroup) {

                var scope = this,
                    sortGroup = [];

                for (var i in result) {
                    sortGroup.push({
                        group: i,
                        length: result[i].length
                    });
                }

                sortGroup.sort(
                    scope.helper.sort(
                        ["length"],
                        false, // false = desc, the most results on top
                        function (a) {
                            return a.toString().toUpperCase()
                        }
                    )
                );

                return $.map(sortGroup, function (val, i) {
                    return val.group
                });
            },
            hint: true,
            href: "{{display}}",
            template: "{{display}}",
            emptyTemplate: "no result for {{query}}",
            source: {
                country: {
                    data: data.countries
                }
            },
            callback: {
                onClickAfter: function (node, a, item, event) {
					event.preventDefault();
					var r = item.href;
					var redirect_url =  $("[data-title='"+r+"']").attr('data-url');

					window.open(redirect_url);
					

                    $('.js-result-container').text('');

                },
                onResult: function (node, query, obj, objCount) {

                
                }
            },
            debug: true
        });

    </script>

<!--   search ends-->

 <?php } 
    
	if(is_page('213')){ do_this_daily();  } 
    if(is_page('249') || is_singular( 'landkreis' ) ){ ?>

<!--    interactive map start-->
 	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/screen.css" media="screen">

  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 
  
      <script type="text/javascript">
	  
	  
      	
    function show_me(name)
    {
		jQuery('#k_'+name).addClass('k_show');
		// jQuery('#accordion .locationinfo').hide();
		$('#karte > span .group-holder').hide();
		   $('#k_'+name+' span.group-holder').css("display", "block"); 
		  
		   $('#k_'+name+' span.group-holder').click(function(){
			   jQuery('#accordion .locationinfo').hide();
				jQuery('#accordion .l_'+name).show();
			});
			
			   $('#k_'+name).click(function(){
			   jQuery('#accordion .locationinfo').hide();
				jQuery('#accordion .l_'+name).show();
			});
    }
    function hide_all()
    {
		//document.getElementById('l_barnim').className = '';
		document.getElementById('k_barnim').className = '';
		//document.getElementById('l_dahme-spree').className = '';
		document.getElementById('k_dahme-spree').className = '';
		//document.getElementById('l_elbe-elster').className = '';
		document.getElementById('k_elbe-elster').className = '';
		//document.getElementById('l_havelland').className = '';
		document.getElementById('k_havelland').className = '';
		//document.getElementById('l_maerkisch-oderland').className = '';
		document.getElementById('k_maerkisch-oderland').className = '';
		//document.getElementById('l_oberhavel').className = '';
		document.getElementById('k_oberhavel').className = '';
		//document.getElementById('l_oberspreewald-lausitz').className = '';
		document.getElementById('k_oberspreewald-lausitz').className = '';
		//document.getElementById('l_oder-spree').className = '';
		document.getElementById('k_oder-spree').className = '';
		//document.getElementById('l_ostprignitz-ruppin').className = '';
		document.getElementById('k_ostprignitz-ruppin').className = '';
		//document.getElementById('l_potsdam-mittelmark').className = '';
		document.getElementById('k_potsdam-mittelmark').className = '';
		//document.getElementById('l_prignitz').className = '';
		document.getElementById('k_prignitz').className = '';
		//document.getElementById('l_spree-neisse').className = '';
		document.getElementById('k_spree-neisse').className = '';
		//document.getElementById('l_teltow-flaeming').className = '';
		document.getElementById('k_teltow-flaeming').className = '';
		//document.getElementById('l_uckermark').className = '';
		document.getElementById('k_uckermark').className = '';
		//document.getElementById('l_brandenburg').className = '';
		document.getElementById('k_brandenburg').className = '';
		//document.getElementById('l_cottbus').className = '';
		document.getElementById('k_cottbus').className = '';
		//document.getElementById('l_frankfurt').className = '';
		document.getElementById('k_frankfurt').className = '';
		//document.getElementById('l_potsdam').className = '';
		document.getElementById('k_potsdam').className = '';
    }
    


	$( document ).ready(function() {
		jQuery('#k_barnim').addClass('k_show');

		   $('#k_barnim span.group-holder').css("display", "block"); 
				jQuery('#accordion .l_barnim').show();
			
			
			
		
	});
		
	
	

    </script>

    <style> #karte span.group-holder{ display:none; } </style>
<!--    interactive map ends-->
    
    
   <?php }?>



<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

var acc = jQuery('.locationinfo h3');
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = '100%';
    } 
  });
}
</script>

<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
	font-weight:bold;
}

.active, .accordion:hover {
    background-color: #ccc;
}

.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

.active:after {
    content: "\2212";
}

.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}
</style>
</html>
