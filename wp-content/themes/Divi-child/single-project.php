<?php

get_header();
 
if( isset($_POST['hidval'])){
if( $_POST['hidval']!=-1){
?>
<script>
window.location.href = "<?php echo $_POST['otherprojectsselect']; ?>";
</script>
<?php 

}
}

$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>
<?php while ( have_posts() ) : the_post(); 
$current_post_id = $post->ID;
$landkreis = get_field('landkreis');
?>

<div id="main-content">                     

    <div class="contact-top-section">
        <div class="container">
            <h1 class="main-title center" style="text-align:center;"> <?php the_title();?> </h1>
            <div class="logo-cat-contact clearfix">
               <?php  $logo = get_field( "logo" );

               if( $logo ) { ?>
                <div class="logo">
                    <img src="<?php echo $logo;?>" />
                </div>
                <?php } ?>

                <div class="contact-person clearfix">
                    <h3>Ansprechpartner</h3>
                    <?php  
                    if( have_rows('name_repeater') ):?>
                    <div class="col-3 col-s-3">
                        <span class="icon-user"></span>
                        <?php
                        while ( have_rows('name_repeater') ) : the_row();
                            $contact_full_name = get_sub_field( "full_name" );
                            $designation = get_sub_field("designation");
                            ?>
                            <div class="name-holder">
                                <span class="name"><?php echo $contact_full_name;?></span>
                                <span><?php echo $designation;?></span>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <?php endif;?>

                    <div class="col-3 col-s-3">
                        <span class="icon-location-map"></span>
                        <?php  
                        $street_and_number = get_field( "street_and_number" );
                        $zip = get_field( "zip" );
                        $city = get_field( "city" );
                        $telefon = get_field( "telefon" );
                        ?>
                        <span><?php echo $street_and_number; ?></span> 
                        <span class="non-block"><?php echo $zip; ?></span> 
                        <span class="non-block"><?php echo $city; ?></span> 
                    </div>

                    <div class="col-3 col-s-3 telefon">

                        <?php 
                        if( have_rows('telefon') ): ?>
                        <span class="icon-phone"></span>
                        <?php
                        echo '<div>'; 
                        while ( have_rows('telefon') ) : the_row(); ?>
                            <span class="block"> <?php the_sub_field('telefon_repeater');?></span>
                        <?php endwhile; 
                        echo '</div>';  endif; ?>

                    </div>


                    <div class="col-3 col-s-3 relative-holder"> 
                        <?php if( have_rows('email_repeater') ):?>
                            <div class="url">
                                <span class="icon-at-the-rate-of"></span>

                                <?php 
                                echo '<span>'; 
                                    // loop through the rows of data
                                while ( have_rows('email_repeater') ) : the_row();

                                        // display a sub field value
                                    $mail = get_sub_field('email');
                                    echo '<a href="mailto:'.$mail.'">'.$mail.'</a>';
                                endwhile;
                                echo '</span>'; 
                                ?>
                            </div>
                        <?php endif;?>

                        <?php if( have_rows('url_repeater') ):?>
                            <div class="globe">
                                <span class="icon-globe"></span>   
                                <?php 
                                echo '<span>'; 
                                    // loop through the rows of data
                                while ( have_rows('url_repeater') ) : the_row();

                                    $url= get_sub_field('url');

                                        // display a sub field value
                                    echo '<a target="_blank" href="'.$url.'">'.$url.'</a>';

                                endwhile;
                                echo '</span>';  
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div> <!--   end .logo-cat-contact-->

            <?php  
            $aktuelle_schwerpunkte = get_field( "aktuelle_schwerpunkte" );

            if( $aktuelle_schwerpunkte ) {
                ?>
                <div class="project-intro-section">
                    <h2 class="center">
                        Aktuelle Schwerpunkte der Bündnisarbeit:
                    </h2>
                    <div class="clearfix">
                        <div class="col-12 desc">
                            <?php echo $aktuelle_schwerpunkte; ?>
                        </div>
                        <!-- .col-6 -->
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php 
    $value = get_field( "statement_name" );
    if($value == " "){ }else { ?>
    <div class="statement" >
        <div class="container">
        <div class="statement-text">
            <?php echo get_field('statement_name');?>
            </div>
        </div>
    </div>
    <?php }?>

<?php if($organisation_des_bundnisses = get_field( "organisation_des_bundnisses" ) || $categories_of_partners = get_field( "categories_of_partners" )){?>
    <div class="text-blocks-container">
        <div class="container clearfix">

            <?php  
            $organisation_des_bundnisses = get_field( "organisation_des_bundnisses" );

            if( $organisation_des_bundnisses ) {
                ?>
                <div class="block1 text-block">
                    <h2>Organisation des Bündnisses</h2>
                    <?php echo $organisation_des_bundnisses; ?>
                </div>
                <?php
            }

            ?>
<?php if ($categories_of_partners = get_field( "categories_of_partners" )){?>
            <div class="kategories block2 text-block">
                <h2><?php the_field('number_of_partners'); ?> Bündnispartner in folgenden Kategorien: </h2>
                <?php  
                $categories_of_partners = get_field( "categories_of_partners" );
                
                if( $categories_of_partners ) {
                    echo '<ul class="styled">';
                    foreach($categories_of_partners as $singlecat){
                        echo '<li>'.$singlecat.'</li>'; 
                        
                    }
                    echo '</ul>'; } ?>
             </div>
        <?php } ?>
        </div>
    </div>
<?php }?>


<?php if($projekte_und_projektvorhaben = get_field( "projekte_und_projektvorhaben" ) || $fruhere_projekte = get_field( "fruhere_projekte" )):?>
    <div class="text-blocks-container bg-grey">
        <div class="container clearfix">

            <?php  
            $projekte_und_projektvorhaben = get_field( "projekte_und_projektvorhaben" );

            if( $projekte_und_projektvorhaben ) {
                ?>
                <div class="block3 text-block">
                    <h2>Projekte und Projektvorhaben</h2>
                    <?php echo $projekte_und_projektvorhaben; ?>
                </div>
                <?php
            }

            ?>

            <?php  
            $fruhere_projekte = get_field( "fruhere_projekte" );

            if( $fruhere_projekte ) {
                ?>
                <div class="block4 text-block">
                    <h2>Frühere Projekte</h2>
                    <?php echo $fruhere_projekte; ?>
                </div>
                <?php
            }

            ?>
        </div>
    </div>
<?php endif;?>

    <div class="gallery-holder clearfix">

        <?php 
        if( have_rows('albums') ):

              $albums = get_field('albums');
              $imagecount =  count($albums);
             


         $counter = 0;
        // loop through the rows of data
         while ( have_rows('albums') ) : the_row();
            $gallery_image = get_sub_field('album_image')['url'];
            $gallery_image_caption = get_sub_field('album_image')['title'];
            echo '<div class="gallery clearfix" style="background: url('.get_sub_field('album_image')['url'].') no-repeat center center; background-size:cover;">';
            echo '<img class="gallery-images" src="'.$gallery_image.'" title="'.$gallery_image_caption.'" alt=""/>';
            $counter++;
            ?>
			<a class="whole-blocker gallery-link<?php echo $counter;?>" href="<?php echo  get_sub_field('album_image')['url']; ?>"></a> 
            <?php $total_image_count = count(get_sub_field('image'))+1; ?>
            <div class="hover-data">
                
                  
              
                    <?php if(get_sub_field('album_name')){?>
                    <span class="album-name"><?php the_sub_field('album_name'); ?></span>
                    <?php }
                 
                    ?>
                    
            </div>
                <?php 
                
                echo '</div>';

            endwhile;
            if($imagecount >= 5){ 
            ?>

             <a href="javascript:void();>" class="more-click">GALERIE ÖFFNEN</a>
            <?php 
            } 
        endif; 

        ?>
        
       
        
    </div>
    <!-- .container -->

    <div class="events">
        <div class="container clearfix">

            <h2 class="center">kommende Veranstaltungen des Bündnisses</h2>
 <?php  $args = array(   'post_type' => 'event',
                                            'posts_per_page' => -1,
                                            'meta_key'          => 'event_date',
                                            'orderby'           => 'meta_value',
                                            'order'             => 'ASC', 
                                            'meta_query' => array(
                                            array(
                                                'key' => 'projects',
                                                'value' => get_the_ID(),
                                                'compare' => '=',       // method of comparison
                                            )
                                        ), 
                                        );
                    $loop = new WP_Query( $args );
                    $my_post_count = $loop->post_count;
                    if( $my_post_count > 0) {?>
            <div class="flexslider carousel">
                <ul class="slides">
                   
                       <?php 
                        while ( $loop->have_posts() ) : $loop->the_post(); ?>  
                    <li>
                        <?php if(get_field('event_date')){
                            // get raw date
                            $date = get_field('event_date', false, false);
                            setlocale(LC_ALL, 'de_DE');
                            ?>
                            <div class="event-date">
                                <span class="month"><?php echo strftime("%b", strtotime($date)); ?></span>
                                <span class="day"><?php echo  strftime("%d", strtotime($date)); ?></span>
                                <span class="year"><?php echo date("Y", strtotime($date));?></span>
                            </div>
                            <?php } ?>
                            <div class="slider-desc">
                                <h4><?php the_title(); ?></h4>
                                <div class="date-info">
                                    <?php if(get_field('start_time')){?>
                                    <span class="time-group">
                                        <span class="icon-time"></span>
                                        <span class="start-time"><?php the_field('start_time');?></span>
                                        <?php if(get_field('start_time') && get_field('end_time')){?>
                                        <span>-</span>
                                        <?php } ?>
                                        <span class="end-time"><?php the_field('end_time');?></span>
                                        <span class="hour">Uhr</span>
                                    </span>
                                    <?php } 
                                    if(get_field('location')){?>
                                    <span class="location-group">
                                        <span class="icon-location-map"></span>
                                        <span class="location"><?php the_field('location');?></span>
                                    </span>
                                    <?php } ?>
                                </div>
                                <?php the_content();?>
                                <a href="<?php the_permalink();?>" class="more-btn"></a>
                            </div>
                            <!-- .slider-desc -->
                    </li>
                    <?php endwhile; ?>   
                </ul>
            </div>
            <?php } else{ echo '<p style="text-align:center;">Derzeit sind keine Veranstaltungen zu diesem Bündnis hinterlegt.<p>';} ?>  
        </div>
    </div>


    <div class="other-projects text-center">
        <div class="container">
            <div class="half-desktop">
                <h2 class="center">weitere Bündnisse des Landkreises<br><?php echo $landkreis->post_title; ?></h2>

                <form name="otherprojects" id="otherprojectsform" class="flexer" action="#" method="post">
                    <div class="custom-select">
                        <select id="otherprojectsselect" name="otherprojectsselect" onchange='changeDropDown(this);'>
                            <option value="-1">Bündnis auswählen</option>

                            <?php
                            
                            $args = array(
                             'post_type'    => 'project',
                             'post__not_in' => array( $current_post_id ),
                             'posts_per_page' => -1,
                             'meta_query' => array(
                             array(
                               'key' => 'landkreis',
                               'value' =>  $landkreis->ID,
                               'compare' => '='
                             )
                              )
                           );
                 
                 
                            $loop = new WP_Query( $args );
                            while ( $loop->have_posts() ) : $loop->the_post(); ?>    

                            <option value="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></option>

                        <?php  endwhile; ?>   
                        </select>
                        <input type="hidden" name="hidval" value="1">
                    </div>
                    <button type="submit" name="send" class="et_pb_button border-color">ZUR BÜNDNISSEITE</button>
                </form>
                <a href="<?php the_permalink(249);?>" class="backtomap et_pb_button border-color">Zurück zur Karte mit Gesamtübersicht</a>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div> <!-- #main-content -->
<?php endwhile; ?>
<?php get_footer(); ?>