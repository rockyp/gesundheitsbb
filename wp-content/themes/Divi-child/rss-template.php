<?php

/**

 * Template Name: Custom RSS feed News Template

 */


$post_per_page= -1;

$post_type= array('post', 'event');





header("Content-type: text/xml"); 

echo "<?xml version='1.0' encoding='UTF-8'?> 

<rss version='2.0'>

<channel>

<title>Rss - Feed </title>



"; 

?>

<?php do_action('rss2_head'); ?>

<?php 



//To fetch post data

$args = array(

	   'post_type' 		=> $post_type,

	   'post_status'	=> 'publish',

	   'posts_per_page' => $post_per_page,

	   'orderby'		=>'date',

	   'order' 			=> 'desc'

	 );

$the_query = new WP_Query( $args ); 



if ( $the_query->have_posts() ) {

while ( $the_query->have_posts() ) : $the_query->the_post();

$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

?><item>		

	<pubDate><?php echo get_the_date('d.m.Y');?></pubDate>

    

	<guid><?php the_permalink_rss(); ?></guid>

	<title><?php the_title_rss(); ?></title>

	<link><?php the_permalink_rss(); ?></link>

   	<description><![CDATA[
		<?php 
		$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
		if($image_attributes[0] != ''){?>
	    <img src="<?php echo $image_attributes[0];?>" width="560px"/> 
		
		<br />

		<?php } the_excerpt(); ?>

		<br>

		<a href="<?php the_permalink_rss(); ?>" style="display:block; background:#000; padding:9px 19px; font-family: 'Lato', sans-serif; font-weight:900; text-transform:uppercase; letter-spacing:1px;">Details...</a>

       

     ]]></description>

	<?php rss_enclosure(); ?>

	<?php do_action('rss2_item'); ?>

	</item>	

<?php endwhile; ?>

<?php } ?>

</channel>

</rss>