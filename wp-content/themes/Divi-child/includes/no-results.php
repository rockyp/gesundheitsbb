<div class="entry">
<!--If no results are found-->
	<h2 class="center" style="margin-top:30px;"><?php esc_html_e('Keine Ergebnisse gefunden','Divi'); ?></h1>
	<p><?php esc_html_e('Leider keine Ergebnisse zu Ihrem Suchbegriff gefunden. Bitte versuchen Sie einen anderen Suchbegriff oder nutzen Sie die Navigation.','Divi'); ?></p>
</div>
<!--End if no results are found-->