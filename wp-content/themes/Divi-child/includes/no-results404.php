<div class="entry">
<!--If no results are found-->
	<h2 style="text-align: center;"><?php esc_html_e('404 Fehlermeldung','Divi'); ?></h2>
	<p style="text-align: center;"><?php esc_html_e('Uups, jetzt ist ein Fehler passiert!','Divi'); ?></p>
	<p><?php esc_html_e('Ergebnisse für Suche nach:','Divi'); ?></p>

	<p><?php esc_html_e('Die von Ihnen gewählte Adresse/URL ist auf unserem Server nicht bzw. nicht mehr vorhanden.','Divi'); ?></p>

	<p><?php esc_html_e('Möglicherweise haben Sie einen veralteten Link bzw. ein altes Bookmark verwendet.','Divi'); ?></p>
	<p><?php esc_html_e('Falls Sie die URL direkt eingegeben haben, ist vielleicht nur ein Tippfehler passiert.','Divi'); ?></p>

	<p><?php esc_html_e('Was können Sie nun tun?','Divi'); ?></p>
	<br/>
	<div class="listing-section">
		<ul>
		    <li><a href="javascript:history.back()"><?php esc_html_e('Zurück zur vorherigen Seite.','Divi'); ?></a></li>
		    <li><?php esc_html_e('Folgen Sie der Navigation oder','Divi'); ?></li>
		    <li><a href="<?php echo home_url(); ?>"><?php esc_html_e('dem Link hier, um zurück zur Homepage zu gelangen!','Divi'); ?></a></li>
		</ul>
	</div>
</div>
<!--End if no results are found-->