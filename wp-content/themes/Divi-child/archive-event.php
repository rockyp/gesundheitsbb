<?php get_header(); ?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div>
				<h2 class="center"><?php the_archive_title(); ?></h2>

				<?php
					$archive_month = get_the_time('m');
					$args2 = array( 
						'post_type' => 'event', 
						'posts_per_page' => -1,
						'post_status'       => 'publish',
						'meta_key'			=> 'event_date',
						'orderby'			=> 'meta_value',
						'order'				=> 'DESC'	
					);

					$the_query = new WP_Query( $args2 );

					if ( $the_query->have_posts() ) : ?>
						<div id="isotope-list" class="spacing-block">
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
								 $post_terms = get_the_terms( get_the_ID(), 'events');
								 $termsString = "";
								 if (is_array($post_terms)) {
									 foreach ( $post_terms as $post_term ) {
									 $termsString .= $post_term->slug.' ';
									}
								}
								$event_complete_date = get_field('event_date');
								$events_months = date("m", strtotime($event_complete_date));

								if($archive_month == $events_months ){ 
							?> 
							<div class="<?php echo $termsString; ?>item"> 
									<?php if(get_field('event_date')){
                        				// get raw date
										$date2 = get_field('event_date', false, false);
										$end_date = get_field('event_end_date', false, false);
										 setlocale(LC_ALL, 'de_DE');
										?>
										<div class="event-date">
											
                                            <span class="month"><?php echo utf8_encode(strftime("%b", strtotime($date2))); ?></span>
                                            <span class="day"><?php echo  strftime("%d", strtotime($date2));  if(get_field('event_end_date')){ ?> - <?php echo  strftime("%d", strtotime($end_date)); } ?></span>
                                            <span class="year"><?php echo date("Y", strtotime($date2));?></span>
                                            
                                     
										</div>
										<?php } ?>
										<div class="slider-desc">
											<h4><?php the_title(); ?></h4>
											<div class="date-info">
												<?php if(get_field('start_time')){?>
												<span class="time-group">
													<span class="icon-time"></span>
													<span class="start-time"><?php 
														if(get_field('event_end_date')){ echo strftime("%d", strtotime($date2)).' '.strftime("%b", strtotime($date2)); ?>,<?php  } ?> 
														<?php the_field('start_time');?>
													</span>
													<?php if(get_field('start_time') && get_field('end_time')){?>
													<span>-</span>

													<?php } ?>
													<span class="end-time"><?php
														if(get_field('event_end_date')){
														echo strftime("%d", strtotime($end_date)).' '.strftime("%b", strtotime($end_date)); ?>,<?php
														} ?>
														<?php the_field('end_time');?>	 	
													</span>
													<span class="hour">Uhr</span>
												</span>
												<?php } 
												if(get_field('location')){?>
												<span class="location-group">
													<span class="icon-location-map"></span>
													<span class="location"><?php the_field('location');?></span>
												</span>
												<?php } ?>
												<span class="cat-group"><?php echo ucfirst($termsString); ?></span>
												</div>
												<?php echo wp_trim_words( get_the_content(), 18, '...');?>
												<a href="<?php the_permalink()?>">Mehr</a>
											</div> <!-- end item -->
							</div> <!-- end item -->
							<?php } endwhile;  ?>
						</div> <!-- end isotope-list -->
					<?php endif; wp_reset_query(); ?>

			</div> <!-- #left-area -->
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>