//Custom Jquery

  jQuery(function($){


    var $container = $('#isotope-list'); //The ID for the list with all the blog posts
     $container.isotope({ //Isotope options, 'item' matches the class in the PHP
     itemSelector : '.item', 
       layoutMode : 'masonry'
     });
     
     //Add the class selected to the item that is clicked, and remove from the others
     var $optionSets = $('#filters'),
     $optionLinks = $optionSets.find('a');
     
     $optionLinks.click(function(){
     var $this = $(this);
     // don't proceed if already selected
     if ( $this.hasClass('selected') ) {
       return false;
     }
     var $optionSet = $this.parents('#filters');
     $optionSets.find('.selected').removeClass('selected');
     $this.addClass('selected');
     
     //When an item is clicked, sort the items.
     var selector = $(this).attr('data-filter');
     $container.isotope({ filter: selector });
     
     return false;
     });


  

jQuery("#otherprojectsselect").select2({
        minimumResultsForSearch: Infinity
    });

jQuery(".gfield_select").select2({
        minimumResultsForSearch: Infinity
    });


  });


//Flex Slider Start
      (function() {

      // store the slider in a local variable
      var $window = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 2 : 2;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      $window.load(function() {
       jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          directionNav: false,
          animationLoop: false,
          itemWidth: 210,
          itemMargin: 50,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    
    
  jQuery( "#otherprojectsselect" ).change(function() {
      var action = jQuery(this).val() ;
     jQuery("#otherprojectsform").attr("action", action);
     
  });


    }());
//Flex Slider Start