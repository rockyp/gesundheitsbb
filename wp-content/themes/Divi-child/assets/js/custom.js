//Custom Jquery

jQuery(document).bind('gform_post_render', function(){
    jQuery('select').not('#otherprojectsselect').niceSelect();
});


  jQuery(function($){


    $('select').not('#otherprojectsselect').niceSelect();

    // jQuery(document).find("#gform_1 .ginput_container_select").addClass('custom-select');

    // SElect js
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var i, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').stop().animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });


    $("header, #et-main-area").click(function(){
        $("#profile-id").hide(600);
        //$('#fixed-holder').find('#profile-id').stop().hide();
    });
 


  $('#info-next').click(function(){
    $(this).parents('#fixed-holder').find('#profile-id').stop().slideToggle();
  });

  $('#fixed-holder .icon-wrap.mail-bg').mouseenter(function(){
    $(this).parents('#fixed-holder').find('#profile-id').stop().hide();
  });

  $('#info-next2').click(function(){
    $(this).parents('#fixed-holder2').find('#profile-id2').stop().slideToggle();
  });

  $('#fixed-holder2 .icon-wrap.mail-bg').mouseenter(function(){
    $(this).parents('#fixed-holder2').find('#profile-id2').stop().hide();
  });

    $('#icon-holder-search').click(function(){
      $(this).parent().parent().toggleClass('open-menu');
      $(this).find('.icon-search').toggleClass('texter');
      // $('.texter').text('close');
      $(this).find('.icon-search:not(.texter)').text('');
      $(this).parent().find('#search-form-header').slideToggle(0);
    });

    function mouseleaveanimate(){
      var widther2 = $('#fixed-holder .icon-wrap.mail-bg').outerWidth() - 59.81;

        $('#fixed-holder .icon-wrap.mail-bg').stop().animate({
          marginRight: -widther2
        }, 600 );

        var widther3 = $('#fixed-holder .icon-wrap.login-bg').outerWidth() - 59.81;

        $('#fixed-holder .icon-wrap.login-bg').stop().animate({
          marginRight: -widther3
        }, 600 );
    }

    mouseleaveanimate();

    jQuery('#fixed-holder .icon-wrap').mouseenter(function(){
      
      $(this).stop().animate({
          marginRight: "0px"
        }, 600 );
    });

    jQuery('#fixed-holder .icon-wrap').mouseleave(function(){
      mouseleaveanimate();
    });


    // init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'masonry',
  getSortData: {
    name: '.name',
    day: '.hidden-date',
    landkreis: '.landkreis'
  },
  sortAscending: {
    landkreis : true,
    name: true
  }
});

$('#toggler-sort').click(function(){
  $('#sorts').slideToggle();
  $(this).toggleClass('open');
});

$('#sorts').click(function(){
  $(this).slideToggle();
})


$('p').filter(function(){
   return this.innerHTML == '&nbsp;';
}).remove();

// bind sort button click
$('#sorts').on( 'click', 'button', function() {
    /* Get the element name to sort */
    var sortValue = $(this).attr('data-sort-value');
    var sortValueText = $(this).text();

    /* Get the sorting direction: asc||desc */
    var sortDirection = $(this).attr('data-sort-direction');

    /* convert it to a boolean */
    sortDirection = sortDirection == 'asc';

    $(this).parent().parent().find('.sort-name').text(function(){
            return sortValueText;
        });;

    $(this).parent().find('.toggler-sort').removeClass('open');

    /* pass it to isotope */
    $grid.isotope({ sortBy: sortValue, sortAscending: sortDirection });


});


    var $container = $('#isotope-list'); //The ID for the list with all the blog posts
     $container.isotope({ //Isotope options, 'item' matches the class in the PHP
     itemSelector : '.item', 
       layoutMode : 'masonry'
     });
     
     //Add the class selected to the item that is clicked, and remove from the others
     var $optionSets = $('#filters'),
     $optionLinks = $optionSets.find('a');
     
     $optionLinks.click(function(){
     var $this = $(this);
     // don't proceed if already selected
     if ( $this.hasClass('selected') ) {
       return false;
     }
     var $optionSet = $this.parents('#filters');
     $optionSets.find('.selected').removeClass('selected');
     $this.addClass('selected');
     
     //When an item is clicked, sort the items.
     var selector = $(this).attr('data-filter');
     $container.isotope({ filter: selector });
     
     return false;
     });


// external js: isotope.pkgd.js




// change is-
  



//     $('.gallery-link').on('click', function () {
//     $(this).next().magnificPopup('open');
// });

$('.gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,
            navigateByImgClick: true
        },
        image: {
             titleSrc: function(item) {
            return item.el.find('.gallery-images').attr('title');
          }
       }
});

$('.more-click').click(function(){
	$('.gallery-link1').click();
});




  });


//Flex Slider Start // 
      (function() {

        // jQuery(".gform_footer").append("<b class='et_submit_button'>Appended text</b>");

      // store the slider in a local variable
      var $window = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 1 :
               (window.innerWidth < 900) ? 2 : 2;
      }

      $window.load(function() {
        if(jQuery('.flexslider').length > 0){
       jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          directionNav: false,
          animationLoop: false,
          itemWidth: 210,
          itemMargin: 50,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
        };

      });

      // check grid size on resize event
      $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    
    
  jQuery( "#otherprojectsselect" ).change(function() {
      var action = jQuery(this).val() ;
     jQuery("#otherprojectsform").attr("action", action);
     
  });


    }());
//Flex Slider Start