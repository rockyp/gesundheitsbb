<?php

get_header();


?>

<div id="main-content">                     

    <div class="contact-top-section">
        <div class="container">
            <h1 class="main-title center" style="text-align:center;"> <?php the_title();?> </h1>
            <?php echo do_shortcode('[project_search_district]');?>
        </div>
    </div>
    <div class="clear"></div>
</div> <!-- #main-content -->
<?php get_footer(); ?>